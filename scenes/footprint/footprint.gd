extends GridCharacter

@export var max_age : int = 5	
var age : int = 0
var tween : Tween

@onready var sprite_2d = $Sprite2D
@onready var label = $Label

# Store the reference to the olf footprint if
# we have already one to support undo
var old_dict_value = null

var dir := Vector2i.RIGHT:
	set(value):
		dir = value
		if sprite_2d:
			sprite_2d.rotation_degrees = directions[value]
		

var directions := {
	Vector2i.RIGHT: 0,
	Vector2i.DOWN: 90,
	Vector2i.LEFT: 180,
	Vector2i.UP: 270,	
}

func _ready():
	sprite_2d.rotation_degrees = directions[dir]
	label.text = ""
	if max_age > 0:
		label.text = str(max_age - age)
	

func _process_step(_old_pos, _new_pos):
	grid.undo_redo.create_action("Aging footprint")
	grid.undo_redo.add_do_property(self, "age", age + 1)
	grid.undo_redo.add_do_method(_do_step)
	grid.undo_redo.add_undo_property(label, "text", label.text)
	grid.undo_redo.add_undo_property(sprite_2d, "modulate:a", sprite_2d.modulate.a)
	grid.undo_redo.add_undo_property(self, "age", age)
	if map_pos in grid.footprint_dict and grid.footprint_dict[map_pos] == self:
		grid.undo_redo.add_undo_method(_reset_footprint_dict)
	grid.undo_redo.add_undo_method(_undo_step)
	grid.undo_redo.commit_action()
	
func _reset_footprint_dict():
	grid.footprint_dict[map_pos] = self
	
func _do_step():
	label.text = str(max_age - age)
	if age == max_age:
		grid.footprints.remove_child(self)
		if grid.footprint_dict[map_pos] == self:
			grid.footprint_dict.erase(map_pos)
	if tween:
		tween.kill()
	if is_inside_tree():
		tween = create_tween()
		tween.tween_property(sprite_2d, "modulate:a", 1 - float(age) / max_age, 1)

func _undo_step():
	if age < max_age and self not in grid.footprints.get_children():
		grid.footprints.add_child(self)
	if tween:
		tween.kill()
	if is_inside_tree():
		tween = create_tween()
		tween.tween_property(sprite_2d, "modulate:a", 1 - float(age) / max_age, 1)

