class_name GridCharacter
extends Node2D



@export var grid : Grid

var map_pos : Vector2i = Vector2i(0, 0):
	set(value):
		if grid:
			map_pos = value
			position = grid.map_to_local(value)
