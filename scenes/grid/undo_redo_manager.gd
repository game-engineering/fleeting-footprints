class_name UndoRedoManager
extends Node

var steps : Array[UndoRedo] = []
var current_step = -1

func new_step():
	# Remove all redoable steps
	steps.resize(current_step + 1)
	steps.append(UndoRedo.new())
	current_step += 1
	
func undo():
	if current_step >= 0:
		var undoredo = steps[current_step]
		while undoredo.has_undo():
			undoredo.undo()
		current_step -= 1
	
func redo():
	if current_step + 1 < steps.size():
		var undoredo = steps[current_step + 1]
		while undoredo.has_redo():
			undoredo.redo()
		current_step += 1
		
func get_undo_redo():
	if current_step >=0:
		return steps[current_step]
