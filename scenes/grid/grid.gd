class_name Grid
extends TileMap

const GREENFLAG = Vector2i(0,1)
const FLOOR = Vector2i(1,0)
const FLOOR_LAYER = 0
const ELEMENTS_LAYER = 1
const GHOSTS_LAYER = 2
const GRAVES_LAYER = 3

const FOOTPRINT = preload("res://scenes/footprint/footprint.tscn")
const GHOST = preload("res://scenes/ghost/ghost.tscn")
const GRAVE = preload("res://scenes/grave/grave.tscn")

@onready var player : Player = $Player
@onready var footprints = $Footprints
@onready var ghosts = $Ghosts
@onready var graves = $Graves

var footprint_dict = {}
var grave_dict = {}

var undo_redo_manager = UndoRedoManager.new()

var undo_redo: UndoRedo = null:
	get:
		return undo_redo_manager.get_undo_redo()

func undo():
	undo_redo_manager.undo()

func redo():
	undo_redo_manager.redo()


func move_right():
	# print("Right")
	player._move_step(Vector2i.RIGHT)

func move_left():
	# print("Left")
	player._move_step(Vector2i.LEFT)

func move_up():
	# print("Up")
	player._move_step(Vector2i.UP)

func move_down():
	# print("Down")
	player._move_step(Vector2i.DOWN)

func _physics_process(_delta):
	if Input.is_action_just_pressed("Down"):
		player._move_step(Vector2i.DOWN)
	if Input.is_action_just_pressed("Up"):
		player._move_step(Vector2i.UP)
	if Input.is_action_just_pressed("Left"):
		player._move_step(Vector2i.LEFT)
	if Input.is_action_just_pressed("Right"):
		player._move_step(Vector2i.RIGHT)
	
func _input(event):
	if event is InputEventScreenDrag:
		var drag : InputEventScreenDrag = event
		player.move_global(drag.position)
	if event is InputEventScreenTouch:
		var touch : InputEventScreenTouch = event
		player.move_global(touch.position)

func _ready():
	player.map_pos = get_used_cells_by_id(ELEMENTS_LAYER, -1, GREENFLAG)[0]
	for pos in get_used_cells(GHOSTS_LAYER):
		var ghost : GridCharacter = GHOST.instantiate()
		ghost.grid = self
		ghost.map_pos = pos
		player.moved.connect(ghost._process_step)
		ghosts.add_child(ghost)
	set_layer_enabled(GHOSTS_LAYER, false)
	for pos in get_used_cells(GRAVES_LAYER):
		var grave : GridCharacter = GRAVE.instantiate()
		grave.grid = self
		grave.map_pos = pos
		graves.add_child(grave)
		grave_dict[pos] = grave
	set_layer_enabled(GRAVES_LAYER, false)
	
func _is_valid_for_move(coords: Vector2i) -> bool:
	if get_cell_atlas_coords(FLOOR_LAYER, coords) != FLOOR:
		return false
	if coords in footprint_dict:
		return true
	return true


func _is_open_grave(pos):
	return pos in grave_dict and not grave_dict[pos].occupied



func _on_player_moved(old_pos, new_pos):
		var fp = FOOTPRINT.instantiate()		
		fp.grid = self
		fp.map_pos = old_pos
		fp.max_age = 10
		if new_pos.x < old_pos.x:
			fp.dir = Vector2i.LEFT
		elif new_pos.y > old_pos.y:
			fp.dir = Vector2i.DOWN
		elif new_pos.y < old_pos.y:
			fp.dir = Vector2i.UP
		
		
		undo_redo.create_action("Instantiate Footprint")
		undo_redo.add_do_method(footprints.add_child.bind(fp))
		undo_redo.add_do_method(_set_fp_dict.bind(fp))
		undo_redo.add_undo_method(_undo_set_fp_dict.bind(fp))
		undo_redo.add_do_reference(fp)
		undo_redo.add_undo_method(footprints.remove_child.bind(fp))
		undo_redo.commit_action()

func _set_fp_dict(fp):
	if fp.map_pos in footprint_dict:
		fp.old_dict_value = footprint_dict[fp.map_pos]
	footprint_dict[fp.map_pos] = fp
	player.moved.connect(fp._process_step)
	
func _undo_set_fp_dict(fp):
	if fp.old_dict_value:
		footprint_dict[fp.map_pos] = fp.old_dict_value
		fp.old_dict_value = null
	else:
		footprint_dict.erase(fp.map_pos)
	player.moved.disconnect(fp._process_step)
