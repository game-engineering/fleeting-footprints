class_name Player
extends GridCharacter

signal moved(old_pos: Vector2i, new_pos: Vector2i)

func move_global(target : Vector2):
	move(grid.local_to_map(target - grid.position))
	
func move(target : Vector2i):
	if not grid._is_valid_for_move(target):
		return
	if target == map_pos:
		return
	var dir = target - map_pos
	# No diagonal movement
	if dir.x != 0 and dir.y != 0:
		return
	# Now it can only be a straight line
	for i in range(dir.length()):
		_move_step(dir.sign())
	
func _move_step(dir : Vector2i):
	if dir.length()!=1:
		print("_move_step with wrong step: ", dir, " / ", dir.length())
		return
	if not grid._is_valid_for_move(map_pos + dir):
		return
	var old_pos = map_pos
	grid.undo_redo_manager.new_step()
	grid.undo_redo.create_action("Player moves")
	grid.undo_redo.add_do_property(self, "map_pos", map_pos + dir)
	grid.undo_redo.add_undo_property(self, "map_pos", map_pos)
	grid.undo_redo.commit_action()
	# print("Moved ", old_pos, " to ", map_pos)
	moved.emit(old_pos, map_pos)
