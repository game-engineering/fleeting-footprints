extends GridCharacter
@onready var sprite_2d = $Sprite2D

@export var occupied = false:
	set(value):
		occupied = value
		if occupied:
			sprite_2d.modulate = Color.html("#7d92c7")
		else:
			sprite_2d.modulate = Color.html("#828282")
