extends GridCharacter


var time = randf_range(1,10)

var dir = Vector2i(0,0)

var wait_steps = 2
var steps_on_field = 0
var entered_grave_at_step = -1
var grave_entered = null

func _process_step(_old_pos, _new_pos):
	grid.undo_redo.create_action("Move ghost")
	grid.undo_redo.add_do_method(_do_step)
	grid.undo_redo.add_undo_property(self, "dir", dir)
	grid.undo_redo.add_undo_property(self, "steps_on_field", steps_on_field)
	grid.undo_redo.add_undo_property(self, "map_pos", map_pos)
	grid.undo_redo.add_undo_method(_undo_step)
	grid.undo_redo.commit_action()
	

func _do_step():
	if grave_entered:
		return
	if map_pos in grid.footprint_dict:
		dir = grid.footprint_dict[map_pos].dir
	else:
		dir = Vector2i(0,0)
		steps_on_field = 0
	if dir:
		steps_on_field += 1
		if steps_on_field >= wait_steps:
			map_pos += dir
			steps_on_field = 0
	if grid._is_open_grave(map_pos):
		grid.grave_dict[map_pos].occupied = true
		entered_grave_at_step = grid.undo_redo_manager.current_step
		grave_entered = grid.grave_dict[map_pos]
		grid.ghosts.remove_child(self)
		
func _undo_step():
	if grid.undo_redo_manager.current_step == entered_grave_at_step:
		grid.ghosts.add_child(self)
		grave_entered.occupied = false
		grave_entered = null
		entered_grave_at_step = -1

func _process(delta):
	time += delta
	scale.x = sin(time) / 5 + 1
	scale.y = cos(time) / 5 + 1
