extends Node2D


@onready var grid : Grid = $Grid
@onready var right_button = $CanvasLayer/RightButton
@onready var left_button = $CanvasLayer/LeftButton
@onready var up_button = $CanvasLayer/UpButton
@onready var down_button = $CanvasLayer/DownButton
@onready var undo_button = $CanvasLayer/UndoButton
@onready var redo_button = $CanvasLayer/RedoButton


func _ready():
	right_button.pressed.connect(grid.move_right)
	left_button.pressed.connect(grid.move_left)
	up_button.pressed.connect(grid.move_up)
	down_button.pressed.connect(grid.move_down)
	undo_button.pressed.connect(grid.undo)
	redo_button.pressed.connect(grid.redo)


func _physics_process(_delta):
	if Input.is_action_just_pressed("undo", true):
		grid.undo()
	elif Input.is_action_just_pressed("redo"):
		grid.redo()
